<?xml version="1.0" encoding="UTF-8"?>

<!-- generated on 2021-05-11 18:49:39 by Eclipse SUMO sumo Version 1.9.1
<configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.dlr.de/xsd/sumoConfiguration.xsd">

    <input>
        <net-file value="/home/klischat/GIT_REPOS/commonroad-scenarios/scenarios/interactive/NGSIM/US101/USA_US101-26_2_I-1-1/USA_US101-26_2_I-1-1.net.xml"/>
        <route-files value="/home/klischat/GIT_REPOS/commonroad-scenarios/scenarios/interactive/NGSIM/US101/USA_US101-26_2_I-1-1/USA_US101-26_2_I-1-1.vehicles.rou.xml,/home/klischat/GIT_REPOS/commonroad-scenarios/scenarios/interactive/NGSIM/US101/USA_US101-26_2_I-1-1/USA_US101-26_2_I-1-1.pedestrians.rou.xml"/>
        <additional-files value="/home/klischat/GIT_REPOS/commonroad-scenarios/scenarios/interactive/NGSIM/US101/USA_US101-26_2_I-1-1/USA_US101-26_2_I-1-1.add.xml"/>
    </input>

    <output>
    </output>

    <time>
        <begin value="0"/>
        <step-length value="0.1"/>
    </time>

    <processing>
        <lateral-resolution value="0.5"/>
        <ignore-route-errors value="true"/>
        <collision.check-junctions value="true"/>
        <time-to-teleport value="-1"/>
        <lanechange.duration value="0"/>
    </processing>

    <report>
        <verbose value="true"/>
        <no-step-log value="true"/>
    </report>

    <traci_server>
        <remote-port value="45071"/>
    </traci_server>

    <random_number>
        <seed value="1234"/>
    </random_number>

    <gui_only>
        <tracker-interval value="0.1"/>
    </gui_only>

</configuration>
-->

<lanechanges>
    <change id="40" type="passenger@40" time="3.50" from="55_1" to="55_0" dir="-1" speed="6.16" pos="44.49" reason="strategic|urgent" leaderGap="8.30" leaderSecureGap="7.25" leaderSpeed="4.53" followerGap="8.37" followerSecureGap="7.20" followerSpeed="6.74" origLeaderGap="30.96" origLeaderSecureGap="0.00" origLeaderSpeed="17.29" latGap="None"/>
    <change id="48" type="passenger@48" time="5.10" from="55_0" to="55_1" dir="1" speed="5.64" pos="41.03" reason="speedGain" leaderGap="7.19" leaderSecureGap="6.08" leaderSpeed="4.98" followerGap="None" followerSecureGap="None" followerSpeed="None" origLeaderGap="18.60" origLeaderSecureGap="6.58" origLeaderSpeed="4.08" latGap="None"/>
    <change id="35" type="passenger@35" time="6.50" from="16_1" to="16_0" dir="-1" speed="6.56" pos="22.56" reason="strategic|urgent" leaderGap="66.48" leaderSecureGap="9.00" leaderSpeed="1.71" followerGap="12.47" followerSecureGap="0.57" followerSpeed="2.75" origLeaderGap="None" origLeaderSecureGap="None" origLeaderSpeed="None" latGap="None"/>
    <change id="18" type="passenger@18" time="8.00" from="16_0" to="16_1" dir="1" speed="0.42" pos="95.39" reason="speedGain" leaderGap="None" leaderSecureGap="None" leaderSpeed="None" followerGap="68.55" followerSecureGap="12.82" followerSpeed="8.46" origLeaderGap="1.38" origLeaderSecureGap="0.42" origLeaderSpeed="0.00" latGap="None"/>
</lanechanges>
